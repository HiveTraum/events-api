from rest_framework import routers

from events.views import EventViewSet
from maps.views import CityViewSet, CountryViewSet
from services.views import RoomViewSet, ServiceViewSet
from subscriptions.views import SubscriptionViewSet
from users.views import UserViewSet

router = routers.DefaultRouter()
router.register(r'events', EventViewSet)
router.register(r'cities', CityViewSet)
router.register(r'users', UserViewSet)
router.register(r'subscriptions', SubscriptionViewSet)
router.register(r'rooms', RoomViewSet)
router.register(r'countries', CountryViewSet)
router.register(r'services', ServiceViewSet)

app_name = "api"

urlpatterns = router.urls
