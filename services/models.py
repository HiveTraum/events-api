from django.db import models
from django.conf import settings

from maps.models import City
from core.abstract import EntityModel


class Room(EntityModel):
    title = models.CharField(max_length=255, verbose_name="Наименование")
    address = models.CharField(max_length=255, verbose_name="Адрес")
    capacity = models.IntegerField(verbose_name="Вместимость")
    city = models.ForeignKey(City, verbose_name="Город", on_delete=models.CASCADE)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name='Владелец', on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Помещение"
        verbose_name_plural = "Помещения"

    def __str__(self):
        return self.city.title + " " + self.title


class Service(EntityModel):
    SHOWMAN = 'SHMN'
    PHOTOGRAPHER = 'PHT'
    DJ = 'DJ'

    CATEGORY = (
        (PHOTOGRAPHER, 'Фотограф'),
        (SHOWMAN, 'Ведущий'),
        (DJ, 'DJ')
    )

    category = models.CharField(max_length=10, verbose_name='Категория', choices=CATEGORY)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name='Исполнитель', related_name='services', on_delete=models.CASCADE)
    price = models.DecimalField(verbose_name='Почасовая оплата', max_digits=8, decimal_places=2)

    class Meta:
        verbose_name = 'Услуга'
        verbose_name_plural = 'Услуги'

    def __str__(self):
        return '{} {} {}'.format(self.owner.last_name, self.owner.first_name, self.category)
