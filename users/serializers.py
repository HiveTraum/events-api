from django.contrib.auth.hashers import make_password

from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from events.models import Event
from services.models import Service

from .models import User

ALL = '__all__'


class PostUserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(required=True, validators=[UniqueValidator(queryset=User.objects.all())])
    password = serializers.CharField(min_length=8, write_only=True)

    class Meta:
        model = User
        fields = ['username', 'email', 'password']

    def validate_password(self, value: str) -> str:
        return make_password(value)


class LightEventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = ['id', 'title', 'image']


class EventInsetListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = ['id', 'title', 'datetime']


class ServiceSerializer(serializers.ModelSerializer):
    category = serializers.SerializerMethodField()

    class Meta:
        model = Service
        fields = ['id', 'category', 'price']

    def get_category(self, obj: Service) -> dict:
        return {
            'id': obj.category,
            'title': obj.get_category_display()
        }


class UserSerializer(serializers.ModelSerializer):
    services = ServiceSerializer(many=True)

    class Meta:
        model = User
        fields = ['id', 'username', 'first_name', 'last_name', 'last_login', 'services', 'events']
        extra_kwargs = {
            'password': {'write_only': True}
        }

    def validate_password(self, value: str) -> str:
        return make_password(value)


class LightUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username']


class UserWithEventsSerializer(serializers.ModelSerializer):
    events = LightEventSerializer(many=True)

    class Meta:
        model = User
        fields = ['id', 'username', 'events']


class UserDetailSerializer(serializers.ModelSerializer):
    events = EventInsetListSerializer(many=True)

    class Meta:
        model = User
        fields = ['id', 'username', 'events']
