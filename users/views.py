from rest_framework import viewsets

from django.middleware.csrf import rotate_token
from django.http.response import HttpResponse

from core.permissions import IsSelfOrReadonly

from .models import User
from .serializers import UserSerializer, PostUserSerializer


# Create your views here.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects
    permission_classes = [IsSelfOrReadonly]

    def get_queryset(self):
        if self.request.method != "GET":
            return self.queryset.all()

        return self.queryset.prefetch_related('events', 'services')

    def get_serializer_class(self):
        if self.request.method == "POST":
            return PostUserSerializer

        return UserSerializer


def csrf(request):
    rotate_token(request)
    return HttpResponse(status=200, content=request.META.get('CSRF_COOKIE'))
