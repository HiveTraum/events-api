import django_filters

from django.db.models import Count, OuterRef, Subquery, Prefetch

from rest_framework import viewsets, filters, exceptions

from core.permissions import IsOwnerOrReadOnly
from subscriptions.models import Subscription

from .models import Event
from .serializers import EventSerializer, EventDetailSerializer, EventListSerializer


class EventFilter(django_filters.FilterSet):
    upcoming = django_filters.BooleanFilter(label=u'Предстоящие')

    class Meta:
        model = Event
        fields = {
            'room__city': ['exact'],
            'owner': ['exact'],
            'datetime': ['gte', 'lte'],
            'upcoming': ['exact']
        }


class EventViewSet(viewsets.ModelViewSet):
    queryset = Event.objects
    permission_classes = [IsOwnerOrReadOnly]
    search_fields = ['title']
    filter_backends = (filters.SearchFilter, django_filters.rest_framework.DjangoFilterBackend, filters.OrderingFilter)
    ordering_fields = ['subscriptions_count']

    def get_queryset(self):
        queryset = self.queryset.all()

        if self.request.method == "GET":
            queryset = self.queryset \
                .select_related("owner", 'room__city__country', "room", "room__city") \
                .annotate(subscriptions_count=Count("subscription"))

            if not self.request.user.is_anonymous:
                subscribed_query = Subscription.objects.filter(user=self.request.user, event=OuterRef("pk"))
                queryset = queryset.annotate(subscription_id=Subquery(subscribed_query.values("pk")))

            pk = self.kwargs.get('pk', None)
            if pk is not None:
                try:
                    sponsor_id = Event.objects.values_list("owner_id", flat=True).get(pk=pk)
                except Event.DoesNotExist:
                    raise exceptions.NotFound()
                prefetch = Prefetch("owner__events",
                                    queryset=Event.objects.filter(sponsor_id=sponsor_id).annotate(
                                        subscriptions_count=Count("subscription")).order_by("subscriptions_count"))

                queryset = queryset.prefetch_related(prefetch)

            return queryset

        return queryset

    def get_serializer_class(self):

        if self.request.method in ['POST', 'PUT', 'PATCH']:
            return EventSerializer

        if self.kwargs.get('pk', None) is not None:
            return EventDetailSerializer

        return EventListSerializer
