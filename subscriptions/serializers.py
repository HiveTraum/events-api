from rest_framework import serializers

from events.models import Event
from users.serializers import LightUserSerializer

from .models import Subscription

ALL = '__all__'


class LightEventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = ['id', 'title', 'image']


class SubscriptionSerializer(serializers.ModelSerializer):
    user = serializers.PrimaryKeyRelatedField(read_only=True, default=serializers.CurrentUserDefault())

    class Meta:
        model = Subscription
        fields = ALL


class SubscriptionHydralizedSerializer(serializers.ModelSerializer):
    user = LightUserSerializer()
    event = LightEventSerializer()

    class Meta:
        model = Subscription
        fields = ALL
