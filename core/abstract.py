from django.db import models


class EntityManager(models.Manager):
    def get_queryset(self):
        return super(EntityManager, self).get_queryset().filter(deleted=False)


class EntityModel(models.Model):
    created = models.DateTimeField(verbose_name="Создано", auto_now_add=True)
    updated = models.DateTimeField(verbose_name="Обновлено", auto_now=True)
    deleted = models.BooleanField(verbose_name="Удалено", default=False)

    objects = EntityManager()

    class Meta:
        abstract = True

    def delete(self, using=None, keep_parents=False, force_delete=False):
        if not force_delete:
            self.deleted = True
            self.save(update_fields=['deleted'])
        else:
            super(EntityModel, self).delete(using=using, keep_parents=keep_parents)
