import hashlib

from django.http.response import HttpResponse, FileResponse, HttpResponseBase
from django.http.request import HttpRequest
from django.conf import settings


def etag_caching_middleware(get_response):
    def middleware(request: HttpRequest) -> HttpResponseBase:

        response = get_response(request)

        if request.method != "GET" or isinstance(response, FileResponse):
            return response

        if_none_match = request.META.get("HTTP_IF_NONE_MATCH", None)
        etag = hashlib.md5(response.content).hexdigest()
        if etag == if_none_match:
            return HttpResponse(status=304)
        response['ETag'] = etag

        return response

    return middleware


def cors(get_response):
    def middleware(request: HttpRequest):
        response = get_response(request)

        if settings.ACCESS_CONTROL_ALLOW_ORIGIN:
            origin = request.META.get('HTTP_ORIGIN')
            if origin in settings.ACCESS_CONTROL_ALLOW_ORIGIN:
                response['Access-Control-Allow-Origin'] = origin
            response['Access-Control-Allow-Methods'] = "GET,PUT,POST,DELETE,OPTIONS"
            response['Access-Control-Allow-Headers'] = "Content-Type, Authorization"
            response['Access-Control-Allow-Credentials'] = 'true'

        return response

    return middleware
